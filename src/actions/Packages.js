import {
  FETCH_ALL_PACKAGES,
  FETCH_ALL_PACKAGES_SUCCESS,
  FETCH_ALL_PACKAGES_BY_USER_ID,
  FETCH_ALL_PACKAGES_BY_USER_ID_SUCCESS,
  ADD_NEW_PACKAGE,
  ADD_NEW_PACKAGE_SUCCESS,
  FETCH_START,
  FETCH_SUCCESS,
  FETCH_ERROR,
} from "constants/ActionTypes";
import axios from "util/Api";
import { handleUserData } from "util/utils";

// export const addNewPackage = (packageData) => {
//   return (dispatch) => {
//     dispatch({ type: FETCH_START });
//     axios
//       .post("users", {
//         firstName,
//         lastName,
//         username,
//         phone,
//         email,
//         password,
//       })
//       .then(({ data }) => {
//         console.log("data:", data);
//         if (data.result) {
//           const token = new Date().getTime();
//           handleUserData({ ...data.result, token });
//           axios.defaults.headers.common["Authorization"] = "Bearer " + token;
//           dispatch({ type: FETCH_SUCCESS });
//           dispatch({ type: USER_TOKEN_SET, payload: token });
//           dispatch({ type: USER_DATA, payload: data.result });
//         } else {
//           console.log("payload: data.error", data.error);
//           dispatch({ type: FETCH_ERROR, payload: "Network Error" });
//         }
//       })
//       .catch(function(error) {
//         dispatch({ type: FETCH_ERROR, payload: error.message });
//         console.log("Error****:", error.message);
//       });
//   };
// };

export const fetchAllPackages = () => {
  return (dispatch) => {
    dispatch({ type: FETCH_START });
    axios
      .get("packages")
      .then(({ data }) => {
        console.log("fetchAllPackages: ", data);
        if (data.result) {
          dispatch({ type: FETCH_SUCCESS });
          dispatch({ type: FETCH_ALL_PACKAGES_SUCCESS, payload: data.result });
        } else {
          dispatch({ type: FETCH_ERROR, payload: data.error });
        }
      })
      .catch(function(error) {
        dispatch({ type: FETCH_ERROR, payload: error.message });
        console.log("Error****:", error.message);
      });
  };
};
