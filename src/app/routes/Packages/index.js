import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ContainerHeader from "components/ContainerHeader/index";
import { fetchAllPackages } from "actions";

const Packages = (props) => {
  const dispatch = useDispatch();
  const { packages } = useSelector((state) => state);

  useEffect(() => {
    dispatch(fetchAllPackages());
  }, []);

  useEffect(() => {
    console.log({ packages });
  }, [packages]);
  return (
    <div className="app-wrapper">
      <ContainerHeader match={props.match} title={"Packages"} />
      <div className="d-flex justify-content-center">
        <h1>This is Packages Page</h1>
      </div>
      {packages.allPackages?.map((pkg) => (
        <p>{JSON.stringify(pkg)}</p>
      ))}
    </div>
  );
};

export default Packages;
