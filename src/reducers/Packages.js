import {
  FETCH_ALL_PACKAGES_SUCCESS,
  FETCH_ALL_PACKAGES_BY_USER_ID_SUCCESS,
  ADD_NEW_PACKAGE_SUCCESS,
} from "../constants/ActionTypes";

const INIT_STATE = {
  allPackages: [],
  allPackagesByUserId: [],
  addedPackage: {},
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_ALL_PACKAGES_SUCCESS: {
      return { ...state, allPackages: action.payload };
    }

    case FETCH_ALL_PACKAGES_BY_USER_ID_SUCCESS: {
      return {
        ...state,
        allPackagesByUserId: action.payload,
      };
    }

    case ADD_NEW_PACKAGE_SUCCESS: {
      return {
        ...state,
        addedPackage: action.payload,
      };
    }

    default:
      return state;
  }
};
