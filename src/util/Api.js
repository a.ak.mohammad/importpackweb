import axios from "axios";

const config = {
  development: {
    API_URL_PREFIX: "http://localhost:7000/api/v1",
  },
  production: {
    API_URL_PREFIX: "http://localhost:7000/api/v1",
  },
};

const AXIOS = axios.create({
  baseURL: config[process.env.NODE_ENV || "development"].API_URL_PREFIX,
  headers: {
    "Content-Type": "application/json",
  },
});

const getData = () => {};
const postData = () => {};
const putData = () => {};
const deleteData = () => {};

export default AXIOS;
