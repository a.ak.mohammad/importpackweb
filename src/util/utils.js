export const handleUserData = (userData) => {
  if (!userData || typeof userData !== "object") return;
  Object.keys(userData).map((key) => localStorage.setItem(key, userData[key]));
};
